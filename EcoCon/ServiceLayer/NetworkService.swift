//
//  NetworkService.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 10.12.2022.
//

import UIKit

protocol NetworkServiceProtocol: AnyObject {
    var onComplitionHomeStore: ((HomeStoreModel) -> Void)? { get set }
    var onComplitionBestSeller: ((BestSellerModel) -> Void)? { get set }
    func fetchWelcomeData(withURL url: String)
}

class NetwortService: NetworkServiceProtocol{
    var onComplitionHomeStore: ((HomeStoreModel) -> Void)?
    var onComplitionBestSeller: ((BestSellerModel) -> Void)?
    
    func fetchWelcomeData(withURL url: String) {
        performRequest(withURLString: url)
    }
    
    fileprivate func performRequest(withURLString urlString: String) {
        guard let url = URL(string: urlString) else { return }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { data, respounse, error in
            if let data = data {
                if let welcomeModel = self.parseJSON(withData: data) {
                    self.onComplitionHomeStore?(welcomeModel.home)
                    self.onComplitionBestSeller?(welcomeModel.best)
                }
            }
        }
        task.resume()
    }
    
    fileprivate func parseJSON(withData data: Data) -> (home: HomeStoreModel, best: BestSellerModel)? {
        let decoder = JSONDecoder()
        do {
            let welcomeData = try decoder.decode(Welcome.self, from: data)
            guard let homeStoreModel = HomeStoreModel(homeStoreModelData: welcomeData),
                  let bestSellerModel = BestSellerModel(BestSallerModelData: welcomeData)
            else {
                return nil
            }
            return (homeStoreModel, bestSellerModel)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
    
}
