//
//  BestSeller.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 13.12.2022.
//

import Foundation

struct BestSellerModel {
    
    var title: [String] = []
    var price: [Int] = []
    var discountPrice: [Int] = []
    var picture: [String] = []
    var isFavorites: [Bool] = []

    init?(BestSallerModelData: Welcome) {
        for index in 0...BestSallerModelData.bestSeller.count - 1 {
            self.title.append(BestSallerModelData.bestSeller[index].title)
            self.price.append(BestSallerModelData.bestSeller[index].priceWithoutDiscount)
            self.picture.append(BestSallerModelData.bestSeller[index].picture)
            self.discountPrice.append(BestSallerModelData.bestSeller[index].discountPrice)
            self.isFavorites.append(BestSallerModelData.bestSeller[index].isFavorites)
        }
    }
}
