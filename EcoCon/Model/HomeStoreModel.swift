//
//  HomeStoreModel.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 10.12.2022.
//

import Foundation

struct HomeStoreModel {
    
    var title: [String] = []
    var subtitle: [String] = []
    var picture: [String] = []
    var isNew: [Bool] = []
    var isBuy: [Bool] = []

    init?(homeStoreModelData: Welcome) {
        for index in 0...homeStoreModelData.homeStore.count - 1 {
            self.title.append(homeStoreModelData.homeStore[index].title)
            self.subtitle.append(homeStoreModelData.homeStore[index].subtitle)
            self.picture.append(homeStoreModelData.homeStore[index].picture)
            self.isNew.append(homeStoreModelData.homeStore[index].isNew ?? false)
            self.isBuy.append(homeStoreModelData.homeStore[index].isBuy)
        }
    }
}
