//
//  MakeMainViewExtension.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 04.03.2023.
//

import UIKit

extension MainViewController {
    func makeMainView(delegate: FilterButtonDelegate) -> UIView {
        let view = MainView()
        view.filterButtonDelegate = delegate
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        return view
    }
}
