//
//  CollectionsDelegate.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 06.12.2022.
//
import UIKit

extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // MARK: Активация выбранной категории
        if collectionView == self.categoriesCollectionView {
            if let cell = collectionView.cellForItem(at: indexPath) as? CategoriesCell {
                cell.changeIconColor(forChange: .active)
            }
            self.hotSalesData = nil
            self.bestSellerData = nil
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.viewModel.updateMainView()
            }
        } else {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        // MARK: Деактивация категории
        if collectionView == self.categoriesCollectionView {
            if let cell = collectionView.cellForItem(at: indexPath) as? CategoriesCell {
                cell.changeIconColor(forChange: .passive)
            }
        } else {
            
        }
    }
}
