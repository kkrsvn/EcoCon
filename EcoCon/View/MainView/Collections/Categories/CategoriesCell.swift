//
//  CategoriesCell.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 06.12.2022.
//

import UIKit

protocol CategoryViewCellProtocol {
    var icon: String { get set }
    var lable: String { get set }
}

protocol CategoriesSetupable {
    func setup(with viewModel: CategoryViewCellProtocol)
}

enum Status {
    case active
    case passive
}

class CategoriesCell: UICollectionViewCell {
    
    struct CategoriesCollection: CategoryViewCellProtocol {
        var icon: String
        var lable: String
    }
    
    private lazy var iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = UIColor(red: 0.702, green: 0.702, blue: 0.765, alpha: 1)
        return imageView
    }()
    
    private lazy var backgroundIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(systemName: "circle.fill", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 71)))
        imageView.tintColor = .white
        imageView.layer.shadowColor = UIColor(red: 0.656, green: 0.669, blue: 0.788, alpha: 0.15).cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowRadius = 20
        return imageView
    }()
    
    private lazy var categoryLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "MarkPro-Medium", size: 12)
        lable.textColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .center
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var imageConstraints = [
        self.backgroundIcon.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: -7),
        self.backgroundIcon.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
        self.iconImage.centerYAnchor.constraint(equalTo: self.backgroundIcon.centerYAnchor),
        self.iconImage.centerXAnchor.constraint(equalTo: self.backgroundIcon.centerXAnchor)
    ]
    
    private lazy var lableConstraints = [
        self.categoryLable.topAnchor.constraint(equalTo: self.backgroundIcon.bottomAnchor),
        self.categoryLable.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor)
    ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.contentView.addSubview(self.backgroundIcon)
        self.contentView.addSubview(self.iconImage)
        self.contentView.addSubview(self.categoryLable)
        self.setupConstraints()
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate(self.imageConstraints)
        NSLayoutConstraint.activate(self.lableConstraints)
    }
    
    func changeIconColor(forChange: Status) {
        switch forChange {
        case .active:
            self.backgroundIcon.tintColor = UIColor(red: 1, green: 0.429, blue: 0.304, alpha: 1)
            self.iconImage.tintColor = .white
        case .passive:
            self.backgroundIcon.tintColor = .white
            self.iconImage.tintColor = UIColor(red: 0.702, green: 0.702, blue: 0.765, alpha: 1)
        }
    }
}

extension CategoriesCell: CategoriesSetupable {
    func setup(with viewModel: CategoryViewCellProtocol) {
        guard let model = viewModel as? CategoriesCollection else { return }
        self.iconImage.image = UIImage(systemName: model.icon, withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 30.0)))
        self.categoryLable.text = model.lable
    }
}
