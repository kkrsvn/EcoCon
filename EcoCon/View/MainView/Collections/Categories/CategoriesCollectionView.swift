//
//  CategoriesCollectionView.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 06.12.2022.
//

import UIKit

extension MainViewController {
    
    func makeCategoriesCollectionLayout() -> UICollectionViewFlowLayout{
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 23
        layout.itemSize = CGSize(width: 71, height: 90)
        return layout
    }
    
    func makeCategoriesCollectionView() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: self.categoriesCollectionLayout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        collection.showsHorizontalScrollIndicator = false
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "DefaultCell")
        collection.register(CategoriesCell.self, forCellWithReuseIdentifier: "CategoriesCell")
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        self.mainView.mainScrollView.addSubview(collection)

        collection.topAnchor.constraint(equalTo: self.mainView.mainScrollView.topAnchor, constant: 63).isActive = true
        collection.trailingAnchor.constraint(equalTo: self.mainView.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        collection.leadingAnchor.constraint(equalTo: self.mainView.mainScrollView.leadingAnchor, constant: 27).isActive = true
        collection.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        return collection
    }
}
