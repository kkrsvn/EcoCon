//
//  HotSalesCollectionExt.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 09.12.2022.
//

import UIKit

extension MainViewController {
    
    func makeHoteSalesLayout() -> UICollectionViewFlowLayout{
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 8
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width - 16, height: 220)
        return layout
    }
    
    func makeHotSalesCollectionView() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: self.hotSalesCollectionLayout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        collection.showsHorizontalScrollIndicator = false
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "DefaultCell")
        collection.register(HotSalesCell.self, forCellWithReuseIdentifier: "HotSalesCell")
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        self.mainView.mainScrollView.addSubview(collection)

        collection.topAnchor.constraint(equalTo: self.mainView.mainScrollView.topAnchor, constant: 280).isActive = true
        collection.trailingAnchor.constraint(equalTo: self.mainView.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        collection.leadingAnchor.constraint(equalTo: self.mainView.mainScrollView.leadingAnchor, constant: 8).isActive = true
        collection.heightAnchor.constraint(equalToConstant: 220).isActive = true
        
        return collection
    }
}
