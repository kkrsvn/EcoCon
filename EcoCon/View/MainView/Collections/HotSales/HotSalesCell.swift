//
//  HotSalesCell.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 09.12.2022.
//

import UIKit

protocol HotSalesViewCellProtocol {
    var title: String { get set }
    var subtitle: String { get set }
    var picture: String { get set }
    var isNew: Bool { get set }
    var isBuy: Bool { get set }
}

protocol HotSalesSetupable {
    func setup(with viewModel: HotSalesViewCellProtocol)
}

class HotSalesCell: UICollectionViewCell {
    
    struct HotSalesCollection: HotSalesViewCellProtocol {
        var title: String
        var subtitle: String
        var picture: String
        var isNew: Bool
        var isBuy: Bool
    }
    
    private lazy var isNewLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "MarkPro-Bold", size: 10)
        lable.text = "New"
        lable.textColor = .white
        lable.numberOfLines = 0
        lable.textAlignment = .center
        lable.isHidden = true
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var backgroundIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(systemName: "circle.fill", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 27)))
        imageView.tintColor = UIColor(red: 1, green: 0.429, blue: 0.304, alpha: 1)
        imageView.isHidden = true
        return imageView
    }()
    
    private lazy var titleLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "MarkPro-Bold", size: 25)
        lable.textColor = .white
        lable.numberOfLines = 2
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var subtitleLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "MarkPro-Regular", size: 11)
        lable.textColor = .white
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var buyButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Buy now!", for: .normal)
        button.titleLabel?.font = UIFont(name: "MarkPro-Regular", size: 11)
        button.backgroundColor = .white
        button.setTitleColor(UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1), for: .normal)
        button.clipsToBounds = true
        button.layer.cornerRadius = 5
        button.isHidden = true
        return button
    }()
    
    private lazy var mainImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var isNewLableConstraints = [
        self.isNewLable.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 23),
        self.isNewLable.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 32),
        self.backgroundIcon.centerYAnchor.constraint(equalTo: self.isNewLable.centerYAnchor),
        self.backgroundIcon.centerXAnchor.constraint(equalTo: self.isNewLable.centerXAnchor)
    ]
    
    private lazy var titlesConstraints = [
        self.titleLable.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 68),
        self.titleLable.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 32),
        self.titleLable.widthAnchor.constraint(equalToConstant: 140),
        self.subtitleLable.topAnchor.constraint(equalTo: self.titleLable.bottomAnchor, constant: 5),
        self.subtitleLable.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 32)
    ]
    
    private lazy var buyButtonConstraints = [
        self.buyButton.topAnchor.constraint(equalTo: self.subtitleLable.bottomAnchor, constant: 26),
        self.buyButton.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 32),
        self.buyButton.heightAnchor.constraint(equalToConstant: 23),
        self.buyButton.widthAnchor.constraint(equalToConstant: 98)
    ]
    
    private lazy var mainImageConstraints = [
        self.mainImage.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0),
        self.mainImage.leadingAnchor.constraint(equalTo: self.titleLable.trailingAnchor, constant: 0),
        self.mainImage.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
        self.mainImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0)
    ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentViewSets()
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.contentView.addSubview(self.backgroundIcon)
        self.contentView.addSubview(self.isNewLable)
        self.contentView.addSubview(self.titleLable)
        self.contentView.addSubview(self.subtitleLable)
        self.contentView.addSubview(self.buyButton)
        self.contentView.addSubview(self.mainImage)
        self.setupConstraints()
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate(self.isNewLableConstraints)
        NSLayoutConstraint.activate(self.titlesConstraints)
        NSLayoutConstraint.activate(self.buyButtonConstraints)
        NSLayoutConstraint.activate(self.mainImageConstraints)
    }
    
    private func contentViewSets() {
        self.contentView.backgroundColor = UIColor(red: 0.063, green: 0.063, blue: 0.063, alpha: 1)
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 16
    }
}

extension HotSalesCell: HotSalesSetupable {
    func setup(with viewModel: HotSalesViewCellProtocol) {
        guard let model = viewModel as? HotSalesCollection else { return }
        self.backgroundIcon.isHidden = !model.isNew
        self.isNewLable.isHidden = !model.isNew
        self.titleLable.text = model.title
        self.subtitleLable.text = model.subtitle
        self.buyButton.isHidden = !model.isBuy
        guard let pictureURL = URL(string: model.picture) else { return }
        self.mainImage.load(url: pictureURL, croping: true)
    }
    
    
}
