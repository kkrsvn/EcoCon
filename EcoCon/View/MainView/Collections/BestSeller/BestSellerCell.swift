//
//  BestSellerCell.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 13.12.2022.
//

import UIKit

protocol BestSellerViewCellProtocol {
    var title: String { get set }
    var discountPrice: Int { get set }
    var price: Int { get set }
    var picture: String { get set }
    var isFavorites: Bool { get set }
}

protocol BestSellerSetupable {
    func setup(with viewModel: BestSellerViewCellProtocol)
}

class BestSellerCell: UICollectionViewCell {
    
    var isFavorite = UIImage(systemName: "heart.fill", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 11)))
    var notFavorite = UIImage(systemName: "heart", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 11)))

    
    struct BestSellerCollection: BestSellerViewCellProtocol {
        var title: String
        var price: Int
        var discountPrice: Int
        var picture: String
        var isFavorites: Bool
    }
    
    private lazy var isFavoritesIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = UIColor(red: 1, green: 0.429, blue: 0.304, alpha: 1)
        imageView.image = notFavorite
        return imageView
    }()
    
    private lazy var backgroundIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(systemName: "circle.fill", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 25)))
        imageView.tintColor = .white
        imageView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15).cgColor
        imageView.layer.shadowOpacity = 1
        imageView.layer.shadowRadius = 20
        return imageView
    }()
    
    private lazy var titleLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "MarkPro-Regular", size: 10)
        lable.textColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var discountPriceLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "MarkPro-Medium", size: 10)
        lable.textColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var priceLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "MarkPro-Bold", size: 16)
        lable.textColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var mainImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var isFavoritesConstraints = [
        self.backgroundIcon.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 11),
        self.backgroundIcon.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -12),
        self.isFavoritesIcon.centerYAnchor.constraint(equalTo: self.backgroundIcon.centerYAnchor),
        self.isFavoritesIcon.centerXAnchor.constraint(equalTo: self.backgroundIcon.centerXAnchor)
    ]
    
    private lazy var titlesConstraints = [
        self.titleLable.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -15),
        self.titleLable.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 21)
    ]
    
    private lazy var priceConstraints = [
        self.priceLable.bottomAnchor.constraint(equalTo: self.titleLable.topAnchor, constant: -5),
        self.priceLable.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 21),
        self.discountPriceLable.centerYAnchor.constraint(equalTo: self.priceLable.centerYAnchor),
        self.discountPriceLable.leadingAnchor.constraint(equalTo: self.priceLable.trailingAnchor, constant: 7)
    ]
    
    private lazy var mainImageConstraints = [
        self.mainImage.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant:  3),
        self.mainImage.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
        self.mainImage.heightAnchor.constraint(equalToConstant: 168),
        self.mainImage.widthAnchor.constraint(equalToConstant: 187)
    ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentViewSets()
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.contentView.addSubview(self.mainImage)
        self.contentView.addSubview(self.titleLable)
        self.contentView.addSubview(self.priceLable)
        self.contentView.addSubview(self.discountPriceLable)
        self.contentView.addSubview(self.backgroundIcon)
        self.contentView.addSubview(self.isFavoritesIcon)
        self.setupConstraints()
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate(self.isFavoritesConstraints)
        NSLayoutConstraint.activate(self.titlesConstraints)
        NSLayoutConstraint.activate(self.priceConstraints)
        NSLayoutConstraint.activate(self.mainImageConstraints)
    }
    
    private func contentViewSets() {
        self.contentView.backgroundColor = .white
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 10
        self.contentView.layer.shadowColor = UIColor(red: 0.667, green: 0.712, blue: 0.829, alpha: 0.1).cgColor
        self.contentView.layer.shadowOpacity = 1
        self.contentView.layer.shadowRadius = 40
    }
}

extension BestSellerCell: BestSellerSetupable {
    func setup(with viewModel: BestSellerViewCellProtocol) {
        guard let model = viewModel as? BestSellerCollection else { return }
        self.titleLable.text = model.title
        self.priceLable.text = "$" + String(model.price)
        self.discountPriceLable.text = "$" + String(model.discountPrice)
        guard let pictureURL = URL(string: model.picture) else { return }
        self.mainImage.load(url: pictureURL, croping: false)
    }
}
