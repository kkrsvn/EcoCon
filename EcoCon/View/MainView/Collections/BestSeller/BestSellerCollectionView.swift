//
//  BestSellerCollectionView.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 13.12.2022.
//

import UIKit

extension MainViewController {
    
    func makeBestSellerLayout() -> UICollectionViewFlowLayout{
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 11
        layout.minimumLineSpacing = 11
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width - 45) / 2, height: 227)
        return layout
    }
    
    func makeBestSellersCollectionView() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: self.bestSellerCollectionLayout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        collection.showsHorizontalScrollIndicator = false
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "DefaultCell")
        collection.register(BestSellerCell.self, forCellWithReuseIdentifier: "BestSellerCell")
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        self.mainView.mainScrollView.addSubview(collection)

        collection.topAnchor.constraint(equalTo: self.mainView.mainScrollView.topAnchor, constant: 558).isActive = true
        collection.trailingAnchor.constraint(equalTo: self.mainView.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: -17).isActive = true
        collection.leadingAnchor.constraint(equalTo: self.mainView.mainScrollView.leadingAnchor, constant: 17).isActive = true
        collection.heightAnchor.constraint(equalToConstant: 465).isActive = true
        
        return collection
    }
}

