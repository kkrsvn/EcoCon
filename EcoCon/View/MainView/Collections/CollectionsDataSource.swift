//
//  CollectionsDataSource.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 06.12.2022.
//

import UIKit

extension MainViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // MARK: - Categories Collection Count
        if collectionView == self.categoriesCollectionView {
            return self.categoriesDataSource.count
        }
        // MARK: - HotSales Collection Count
        else if collectionView == self.hotSalesCollectionView {
            return self.hotSalesData?.title.count ?? 1
        }
        // MARK: - BestSeller Collection Count
        else {
            return self.bestSellerData?.title.count ?? 4
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // MARK: - Categories Collection View
        if collectionView == self.categoriesCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as? CategoriesCell else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultCell", for: indexPath)
                cell.backgroundColor = .systemRed
                return cell
            }
            let image = self.categoriesDataSource[indexPath.row].image
            let text = self.categoriesDataSource[indexPath.row].lable
            let viewModel = CategoriesCell.CategoriesCollection(icon: image, lable: text)
            cell.setup(with: viewModel)
            return cell
        }
        // MARK: - Hot Sales Collection View
        else if collectionView == self.hotSalesCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HotSalesCell", for: indexPath) as? HotSalesCell,
                  self.hotSalesData != nil
            else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultCell", for: indexPath)
                cell.backgroundColor = UIColor(red: 0.702, green: 0.702, blue: 0.765, alpha: 1)
                return cell
            }
            
            let title: String = self.hotSalesData?.title[indexPath.row] ?? ""
            let subtitle: String = self.hotSalesData?.subtitle[indexPath.row] ?? ""
            let picture: String = self.hotSalesData?.picture[indexPath.row] ?? ""
            let isNew: Bool = self.hotSalesData?.isNew[indexPath.row] ?? false
            let isBuy: Bool = self.hotSalesData?.isBuy[indexPath.row] ?? false
            let viewModel = HotSalesCell.HotSalesCollection(title: title, subtitle: subtitle, picture: picture, isNew: isNew, isBuy: isBuy)
            cell.setup(with: viewModel)
            return cell
        }
        // MARK: - Best Seller Collection View
        else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestSellerCell", for: indexPath) as? BestSellerCell,
                  self.bestSellerData != nil
            else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultCell", for: indexPath)
                cell.backgroundColor = UIColor(red: 0.702, green: 0.702, blue: 0.765, alpha: 1)
                return cell
            }
            
            let title: String = self.bestSellerData?.title[indexPath.row] ?? ""
            let picture: String = self.bestSellerData?.picture[indexPath.row] ?? ""
            let price: Int = self.bestSellerData?.price[indexPath.row] ?? 0
            let discountPrice: Int = self.bestSellerData?.discountPrice[indexPath.row] ?? 0
            let isFavorites = self.bestSellerData?.isFavorites[indexPath.row] ?? false
            
            let viewModel = BestSellerCell.BestSellerCollection(title: title, price: price, discountPrice: discountPrice, picture: picture, isFavorites: isFavorites)
            cell.setup(with: viewModel)
            return cell
        }
    }
}
