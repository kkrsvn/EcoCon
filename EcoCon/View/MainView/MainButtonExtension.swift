//
//  MainButtonExtension.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 04.03.2023.
//

import UIKit

protocol FilterButtonDelegate: AnyObject {
    func presentFilterView()
}

extension MainViewController: FilterButtonDelegate {
    func presentFilterView() {
        let filterView = FilterViewController()
        filterView.modalPresentationStyle = .pageSheet
        filterView.sheetPresentationController?.detents = [.medium()]
        self.present(filterView, animated: true, completion: nil)
    }
}
