//
//  MakeExplorerViewExtension.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 04.03.2023.
//

import UIKit

extension MainViewController {
    func makeExplorerView() -> UIView {
        let view = ExplorerView()
        view.backgroundColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        view.clipsToBounds = true
        view.layer.cornerRadius = 30
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        view.heightAnchor.constraint(equalToConstant: 72).isActive = true
        return view
    }
}
