//
//  ExplorerView.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 04.03.2023.
//

import UIKit

class ExplorerView: UIView {
    
    // MARK: Constants
    var indentExplorerItem = (UIScreen.main.bounds.width - 135) / 5
    
    // MARK: Explorer Icon
    private var explorerIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .white
        imageView.image = UIImage(systemName: "circle.fill", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 8)))
        return imageView
    }()
    
    private lazy var explorerIconConstraints = [
        self.explorerIcon.centerYAnchor.constraint(equalTo: self.centerYAnchor),
        self.explorerIcon.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: self.indentExplorerItem)
    ]
    
    // MARK: Explorer Lable
    private var explorerLable: UILabel = {
        let lable = UILabel()
        lable.text = "Explorer"
        lable.font = UIFont(name: "MarkPro-Bold", size: 15)
        lable.textColor = .white
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var explorerLableConstraints = [
        self.explorerLable.centerYAnchor.constraint(equalTo: self.centerYAnchor),
        self.explorerLable.leadingAnchor.constraint(equalTo: self.explorerIcon.trailingAnchor, constant: 7)
    ]
    
    // MARK: Bag Icon
    private var bagIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .white
        imageView.image = UIImage(systemName: "bag", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 18)))
        return imageView
    }()
    
    private lazy var bagIconConstraints = [
        self.bagIcon.centerYAnchor.constraint(equalTo: self.centerYAnchor),
        self.bagIcon.leadingAnchor.constraint(equalTo: self.explorerLable.trailingAnchor, constant: self.indentExplorerItem)
    ]
    
    // MARK: <3 Icon
    private var favoritesIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .white
        imageView.image = UIImage(systemName: "heart", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 18)))
        return imageView
    }()
    
    private lazy var favoritesIconConstraints = [
        self.favoritesIcon.centerYAnchor.constraint(equalTo: self.centerYAnchor),
        self.favoritesIcon.leadingAnchor.constraint(equalTo: self.bagIcon.trailingAnchor, constant: self.indentExplorerItem)
    ]
    
    // MARK: Profile Icon
    private var profilegIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .white
        imageView.image = UIImage(systemName: "bag", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 18)))
        return imageView
    }()
    
    private lazy var profilegIconConstraints = [
        self.profilegIcon.centerYAnchor.constraint(equalTo: self.centerYAnchor),
        self.profilegIcon.leadingAnchor.constraint(equalTo: self.favoritesIcon.trailingAnchor, constant: self.indentExplorerItem)
    ]
    
    // MARK: - MainView Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - MainView Setup
    private func setupView() {
        self.backgroundColor = .white
        self.addedSubviews()
        self.setupConstraints()
    }
    
    // MARK: - AddSubview functions
    private func addedSubviews() {
        self.addSubview(self.explorerIcon)
        self.addSubview(self.explorerLable)
        self.addSubview(self.bagIcon)
        self.addSubview(self.favoritesIcon)
        self.addSubview(self.profilegIcon)
    }
    
    // MARK: - Constraints Activation
    private func setupConstraints() {
        NSLayoutConstraint.activate(self.explorerIconConstraints)
        NSLayoutConstraint.activate(self.explorerLableConstraints)
        NSLayoutConstraint.activate(self.bagIconConstraints)
        NSLayoutConstraint.activate(self.favoritesIconConstraints)
        NSLayoutConstraint.activate(self.profilegIconConstraints)
    }
}
