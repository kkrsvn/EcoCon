//
//  MainViewController.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 28.02.2023.
//

import UIKit

class MainViewController: UIViewController {
    
    // MARK: Defaults
    var viewModel: MainViewModelProtocol!
    var mainView: MainView!
    var explorerView: ExplorerView!
    var categoriesDataSource: [(image: String, lable: String)]!
    var categoriesCollectionLayout: UICollectionViewFlowLayout!
    var categoriesCollectionView: UICollectionView!
    var hotSalesCollectionLayout: UICollectionViewFlowLayout!
    var hotSalesCollectionView: UICollectionView!
    var bestSellerCollectionLayout: UICollectionViewFlowLayout!
    var bestSellerCollectionView: UICollectionView!
    
    // MARK: Отслеживание обновления данных и перезагрузка коллекций
    var hotSalesData: HomeStoreModel? {
        didSet {
            DispatchQueue.main.async {
                self.hotSalesCollectionView.reloadData()
            }
        }
    }
    var bestSellerData: BestSellerModel? {
        didSet {
            DispatchQueue.main.async {
                self.bestSellerCollectionView.reloadData()
            }
        }
    }
    //MARK: ViewController Init
    required init(viewModel: MainViewModelProtocol, categories: [(image: String, lable: String)]) {
        self.viewModel = viewModel
        self.categoriesDataSource = categories
        super .init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Загрузка View
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //MARK: Появление View
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.activateFirstCategoriesIcon()
    }
    
    // MARK: Получение данных HotSalesCollection
    private func getHomeStoreProducts() {
        self.viewModel.networkService.onComplitionHomeStore = { [weak self] viewData in
            self?.hotSalesData = viewData
        }
    }
    
    // MARK: Получение данных BestSellerCollection
    private func getBestSellerProducts() {
        self.viewModel.networkService.onComplitionBestSeller = { [weak self] viewData in
            self?.bestSellerData = viewData
        }
    }
    
    // MARK: Первоначальная настройка View
    private func setupView() {
        self.makeUpCollections()
        self.viewModel.updateMainView()
        self.getHomeStoreProducts()
        self.getBestSellerProducts()
    }

    // MARK: Cоздание коллекций и MainView
    private func makeUpCollections() {
        self.mainView = self.makeMainView(delegate: self) as? MainView
        self.explorerView = self.makeExplorerView() as? ExplorerView
        self.categoriesCollectionLayout = self.makeCategoriesCollectionLayout()
        self.categoriesCollectionView = self.makeCategoriesCollectionView()
        self.hotSalesCollectionLayout = self.makeHoteSalesLayout()
        self.hotSalesCollectionView = self.makeHotSalesCollectionView()
        self.bestSellerCollectionLayout = self.makeBestSellerLayout()
        self.bestSellerCollectionView = self.makeBestSellersCollectionView()
    }
    
    // MARK: Активация первой категории (по умолчанию)
    private func activateFirstCategoriesIcon() {
        let indexPathForFirstRow = IndexPath(row: 0, section: 0)
        self.categoriesCollectionView.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: [])
        self.collectionView(self.categoriesCollectionView, didSelectItemAt: indexPathForFirstRow)
    }
}


