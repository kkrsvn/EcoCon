//
//  MainView.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 28.02.2023.
//

import UIKit

class MainView: UIView {
    
    weak var filterButtonDelegate: FilterButtonDelegate?
    
    // MARK: - ScrollView
    lazy var mainScrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsVerticalScrollIndicator = false
        scroll.backgroundColor = UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.contentSize.height = 1030
        return scroll
    }()
    
    private lazy var scrollViewConstraints = [
        self.mainScrollView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
        self.mainScrollView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        self.mainScrollView.topAnchor.constraint(equalTo: self.topAnchor, constant: 100),
        self.mainScrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -72)
    ]
    
    // MARK: - Filter Button
    private lazy var filterButton: UIButton = {
        let image = UIImage(named: "filter")
        
        let button = UIButton()
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.didTapFilterButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private lazy var filterButtonConstraints = [
        self.filterButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 67),
        self.filterButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -35)
    ]
    
    // MARK: - Category Title
    private var categoryTitleLable: UILabel = {
        let lable = UILabel()
        lable.text = "Select Category"
        lable.font = UIFont(name: "MarkPro-Bold", size: 25)
        lable.textColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var categoryTitleLableConstraints = [
        self.categoryTitleLable.topAnchor.constraint(equalTo: self.mainScrollView.topAnchor, constant: 0),
        self.categoryTitleLable.leadingAnchor.constraint(equalTo: self.mainScrollView.leadingAnchor, constant: 17)
    ]
    
    // MARK: - Category ViewAll Lable
    private var categoryViewAllLable: UILabel = {
        let lable = UILabel()
        lable.text = "view all"
        lable.font = UIFont(name: "MarkPro-Regular", size: 15)
        lable.textColor = UIColor(red: 1, green: 0.429, blue: 0.304, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var categoryViewAllLableConstraints = [
        self.categoryViewAllLable.centerYAnchor.constraint(equalTo: self.categoryTitleLable.centerYAnchor),
        self.categoryViewAllLable.trailingAnchor.constraint(equalTo: self.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: -33)
    ]
    
    // MARK: - Search Background View
    private var backgroundSearchView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 17
        imageView.layer.shadowColor = UIColor(red: 0.75, green: 0.771, blue: 0.962, alpha: 0.15).cgColor
        imageView.layer.shadowOpacity = 1
        imageView.layer.shadowRadius = 20
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var backgroundSearchViewConstraints = [
        self.backgroundSearchView.topAnchor.constraint(equalTo: self.mainScrollView.topAnchor, constant: 188),
        self.backgroundSearchView.leadingAnchor.constraint(equalTo: self.mainScrollView.leadingAnchor, constant: 32),
        self.backgroundSearchView.trailingAnchor.constraint(equalTo: self.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: -82),
        self.backgroundSearchView.heightAnchor.constraint(equalToConstant: 34)
    ]
    
    // MARK: - Search Icon
    private var searchIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = UIColor(red: 1, green: 0.429, blue: 0.304, alpha: 1)
        imageView.image = UIImage(systemName: "magnifyingglass", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 13.52)))
        return imageView
    }()
    
    private lazy var searchIconConstraints = [
        self.searchIcon.topAnchor.constraint(equalTo: self.backgroundSearchView.topAnchor, constant: 9),
        self.searchIcon.leadingAnchor.constraint(equalTo: self.backgroundSearchView.leadingAnchor, constant: 24)
    ]
    
    // MARK: - Search TextField
    private var searchTextField: UITextField = {
        let textField = UITextField()
        textField.font = UIFont(name: "MarkPro-Regular", size: 12)
        textField.textColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        textField.backgroundColor = .clear
        textField.placeholder = "Search"
        textField.autocapitalizationType = .none
        textField.tintColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 0.5)
        textField.clearButtonMode = UITextField.ViewMode.never
        textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private lazy var searchTextFieldConstraints = [
        self.searchTextField.centerYAnchor.constraint(equalTo: self.backgroundSearchView.centerYAnchor),
        self.searchTextField.leadingAnchor.constraint(equalTo: self.mainScrollView.leadingAnchor, constant: 74),
        self.searchTextField.trailingAnchor.constraint(equalTo: self.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: -124)
    ]
    
    // MARK: - Search Tools Icon
    private var searchToolsIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "search_tool.png")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var searchToolsIconConstraints = [
        self.searchToolsIcon.centerYAnchor.constraint(equalTo: self.backgroundSearchView.centerYAnchor),
        self.searchToolsIcon.trailingAnchor.constraint(equalTo: self.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: -18.5),
        self.searchToolsIcon.heightAnchor.constraint(equalToConstant: 68),
        self.searchToolsIcon.widthAnchor.constraint(equalToConstant: 68)
    ]
    
    // MARK: - Hot Sales Title
    private var hotSalesTitleLable: UILabel = {
        let lable = UILabel()
        lable.text = "Hot Sales"
        lable.font = UIFont(name: "MarkPro-Bold", size: 25)
        lable.textColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var hotSalesTitleLableConstraints = [
        self.hotSalesTitleLable.topAnchor.constraint(equalTo: self.backgroundSearchView.bottomAnchor, constant: 24),
        self.hotSalesTitleLable.leadingAnchor.constraint(equalTo: self.mainScrollView.leadingAnchor, constant: 17)
    ]
    
    // MARK: - Hot Sales ViewAll Lable
    private var hotSalesViewAllLable: UILabel = {
        let lable = UILabel()
        lable.text = "see more"
        lable.font = UIFont(name: "MarkPro-Regular", size: 15)
        lable.textColor = UIColor(red: 1, green: 0.429, blue: 0.304, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var hotSalesViewAllLableConstraints = [
        self.hotSalesViewAllLable.centerYAnchor.constraint(equalTo: self.hotSalesTitleLable.centerYAnchor),
        self.hotSalesViewAllLable.trailingAnchor.constraint(equalTo: self.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: -27)
    ]
    
    // MARK: - Best Seller Title
    private var bestSellerTitleLable: UILabel = {
        let lable = UILabel()
        lable.text = "Best Seller"
        lable.font = UIFont(name: "MarkPro-Bold", size: 25)
        lable.textColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var bestSellerTitleLableConstraints = [
        self.bestSellerTitleLable.topAnchor.constraint(equalTo: self.hotSalesTitleLable.bottomAnchor, constant: 237),
        self.bestSellerTitleLable.leadingAnchor.constraint(equalTo: self.mainScrollView.leadingAnchor, constant: 17)
    ]
    
    // MARK: - Best Seller ViewAll Lable
    private var bestSellerViewAllLable: UILabel = {
        let lable = UILabel()
        lable.text = "see more"
        lable.font = UIFont(name: "MarkPro-Regular", size: 15)
        lable.textColor = UIColor(red: 1, green: 0.429, blue: 0.304, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var bestSellerViewAllLableConstraints = [
        self.bestSellerViewAllLable.centerYAnchor.constraint(equalTo: self.bestSellerTitleLable.centerYAnchor),
        self.bestSellerViewAllLable.trailingAnchor.constraint(equalTo: self.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: -27)
    ]
    
    // MARK: - MainView Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - MainView Setup
    private func setupView() {
        self.backgroundColor = .white
        self.addedMainViewSubviews()
        self.addedScrollViewSubviews()
        self.setupConstraints()
    }
    
    // MARK: - AddSubview functions
    private func addedMainViewSubviews() {
        self.addSubview(self.mainScrollView)
        self.addSubview(self.filterButton)
    }
    
    private func addedScrollViewSubviews() {
        self.mainScrollView.addSubview(self.categoryTitleLable)
        self.mainScrollView.addSubview(self.categoryViewAllLable)
        self.mainScrollView.addSubview(self.backgroundSearchView)
        self.mainScrollView.addSubview(self.searchIcon)
        self.mainScrollView.addSubview(self.searchTextField)
        self.mainScrollView.addSubview(self.searchToolsIcon)
        self.mainScrollView.addSubview(self.hotSalesTitleLable)
        self.mainScrollView.addSubview(self.hotSalesViewAllLable)
        self.mainScrollView.addSubview(self.bestSellerTitleLable)
        self.mainScrollView.addSubview(self.bestSellerViewAllLable)
    }
    
    // MARK: - Constraints Activation
    private func setupConstraints() {
        NSLayoutConstraint.activate(self.filterButtonConstraints)
        NSLayoutConstraint.activate(self.scrollViewConstraints)
        NSLayoutConstraint.activate(self.categoryTitleLableConstraints)
        NSLayoutConstraint.activate(self.categoryViewAllLableConstraints)
        NSLayoutConstraint.activate(self.backgroundSearchViewConstraints)
        NSLayoutConstraint.activate(self.searchIconConstraints)
        NSLayoutConstraint.activate(self.searchTextFieldConstraints)
        NSLayoutConstraint.activate(self.searchToolsIconConstraints)
        NSLayoutConstraint.activate(self.hotSalesTitleLableConstraints)
        NSLayoutConstraint.activate(self.hotSalesViewAllLableConstraints)
        NSLayoutConstraint.activate(self.bestSellerTitleLableConstraints)
        NSLayoutConstraint.activate(self.bestSellerViewAllLableConstraints)
    }
    
    // MARK: - FilterButton function
    @objc private func didTapFilterButton() {
        self.filterButtonDelegate?.presentFilterView()
    }
}
