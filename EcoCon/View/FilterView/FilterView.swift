//
//  FilterView.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 05.03.2023.
//

import UIKit

class FilterView: UIView {
    
    let brandTitles: [String] = ["Samsung", "Xiaomi", "Motorola"]
    let priceTitles: [String] = ["$300 - $500", "$500 - $1000", "$1000 - $1500"]
    let sizeTitles: [String] = ["4.5 to 5.5 inches", "5.5 to 6.0 inches", "6.0 to 6.5 inches"]
    
    weak var cancelButtonDelegate: CancelButtonDelegate?
    weak var doneButtonDelegate: DoneButtonDelegate?
    
    var brandLable: UILabel!
    var brandSelectedLable: UILabel!
    var brandDropDownButton: UIButton!
    var brandDropDownMenu: UIMenu!
    
    var priceLable: UILabel!
    var priceSelectedLable: UILabel!
    var priceDropDownButton: UIButton!
    var priceDropDownMenu: UIMenu!
    
    var sizeLable: UILabel!
    var sizeSelectedLable: UILabel!
    var sizeDropDownButton: UIButton!
    var sizeDropDownMenu: UIMenu!

    private lazy var titleLable: UILabel = {
        let lable = UILabel()
        lable.text = "Filter options"
        lable.font = UIFont(name: "MarkPro-Medium", size: 18)
        lable.textColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var titleLableConstraints = [
        self.titleLable.topAnchor.constraint(equalTo: self.topAnchor, constant: 31),
        self.titleLable.centerXAnchor.constraint(equalTo: self.centerXAnchor)
    ]
    
    private lazy var cancelButton: UIButton = {
        let image = UIImage(systemName: "xmark", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 14.14)))
        
        let button = UIButton()
        button.backgroundColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        button.setImage(image, for: .normal)
        button.imageView?.tintColor = .white
        button.clipsToBounds = true
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(self.tapCancelButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var cancelButtonConstraints = [
        self.cancelButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 44),
        self.cancelButton.centerYAnchor.constraint(equalTo: self.titleLable.centerYAnchor),
        self.cancelButton.widthAnchor.constraint(equalToConstant: 37),
        self.cancelButton.heightAnchor.constraint(equalToConstant: 37)
    ]
    
    private lazy var doneButton: UIButton = {
        let attrTitle = NSAttributedString(string: "Done", attributes: [NSAttributedString.Key.font: UIFont(name: "MarkPro-Medium", size: 18)!])
        
        let button = UIButton()
        button.backgroundColor = UIColor(red: 1, green: 0.429, blue: 0.304, alpha: 1)
        button.setTitleColor(.white, for: .normal)
        button.setAttributedTitle(attrTitle, for: UIControl.State.normal)
        button.clipsToBounds = true
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(self.tapDoneButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var doneButtonConstraints = [
        self.doneButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
        self.doneButton.centerYAnchor.constraint(equalTo: self.titleLable.centerYAnchor),
        self.doneButton.widthAnchor.constraint(equalToConstant: 86),
        self.doneButton.heightAnchor.constraint(equalToConstant: 37)
    ]
    
    // MARK: - MainView Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - MainView Setup
    private func setupView() {
        self.backgroundColor = .white
        self.clipsToBounds = true
        self.layer.cornerRadius = 30
        self.addedubviews()
        self.setupConstraints()
        self.brandUISets()
        self.priceUISets()
        self.sizeUISets()
    }
    
    // MARK: - AddSubview functions
    private func addedubviews() {
        self.addSubview(self.titleLable)
        self.addSubview(self.cancelButton)
        self.addSubview(self.doneButton)
    }
    
    // MARK: - Constraints Activation
    private func setupConstraints() {
        NSLayoutConstraint.activate(self.titleLableConstraints)
        NSLayoutConstraint.activate(self.cancelButtonConstraints)
        NSLayoutConstraint.activate(self.doneButtonConstraints)
    }

    private func brandUISets() {
        self.brandSelectedLable = self.makeSelectedLable(defaultLable: self.brandTitles.first!, topAnchor: 133)
        self.brandDropDownMenu = self.makeDropDownMenu(titles: brandTitles, selectedLable: self.brandSelectedLable)
        self.brandDropDownButton = self.makeDropDownButton(menu: self.brandDropDownMenu, selectedLable: self.brandSelectedLable)
        self.brandLable = self.makeLable(title: "Brand", bottomAnchor: self.brandSelectedLable)
    }
    
    private func priceUISets() {
        self.priceSelectedLable = self.makeSelectedLable(defaultLable: self.priceTitles.first!, topAnchor: 212)
        self.priceDropDownMenu = self.makeDropDownMenu(titles: priceTitles, selectedLable: self.priceSelectedLable)
        self.priceDropDownButton = self.makeDropDownButton(menu: self.priceDropDownMenu, selectedLable: self.priceSelectedLable)
        self.priceLable = self.makeLable(title: "Price", bottomAnchor: self.priceSelectedLable)
    }
    
    private func sizeUISets() {
        self.sizeSelectedLable = self.makeSelectedLable(defaultLable: self.sizeTitles.first!, topAnchor: 294)
        self.sizeDropDownMenu = self.makeDropDownMenu(titles: sizeTitles, selectedLable: self.sizeSelectedLable)
        self.sizeDropDownButton = self.makeDropDownButton(menu: self.sizeDropDownMenu, selectedLable: self.sizeSelectedLable)
        self.sizeLable = self.makeLable(title: "Size", bottomAnchor: self.sizeSelectedLable)
    }
    
    @objc private func tapCancelButton() {
        self.cancelButtonDelegate?.closeFilterView()
    }
    
    @objc private func tapDoneButton() {
        self.doneButtonDelegate?.acceptFilter()
    }

}
