//
//  FilterViewExtansion.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 05.03.2023.
//

import UIKit

extension UIView {
    func makeLable(title: String, bottomAnchor: UILabel) -> UILabel {
        let lable = UILabel()
        lable.text = title
        lable.font = UIFont(name: "MarkPro-Medium", size: 18)
        lable.textColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(lable)
        
        lable.bottomAnchor.constraint(equalTo: bottomAnchor.topAnchor, constant: -7).isActive = true
        lable.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 46).isActive = true
        
        return lable
    }
    
    func makeSelectedLable(defaultLable: String, topAnchor: CGFloat) -> UILabel {
        let lable = UILabel()
        lable.text = "   " + defaultLable
        lable.font = UIFont(name: "MarkPro-Regular", size: 18)
        lable.textColor = UIColor(red: 0.004, green: 0, blue: 0.208, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.layer.borderWidth = 1
        lable.layer.borderColor = UIColor(red: 0.863, green: 0.863, blue: 0.863, alpha: 1).cgColor
        lable.clipsToBounds = true
        lable.layer.cornerRadius = 5
        lable.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(lable)
        
        lable.topAnchor.constraint(equalTo: self.topAnchor, constant: topAnchor).isActive = true
        lable.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 46).isActive = true
        lable.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -31).isActive = true
        lable.heightAnchor.constraint(equalToConstant: 37).isActive = true
        
        return lable
    }
    
    func makeDropDownButton(menu: UIMenu, selectedLable: UILabel) -> UIButton {
        
        let image = UIImage(systemName: "control", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 20)))
        
        let button = UIButton()
        button.setImage(image, for: .normal)
        
        if let image = button.imageView {
            button.imageView?.transform = image.transform.rotated(by: .pi)
            button.imageView?.tintColor = UIColor(red: 0.702, green: 0.702, blue: 0.702, alpha: 1)
        }
        
        button.clipsToBounds = true
        button.layer.cornerRadius = 10
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.menu = menu
        button.showsMenuAsPrimaryAction = true
        
        self.addSubview(button)
        
        button.trailingAnchor.constraint(equalTo: selectedLable.trailingAnchor, constant: -15).isActive = true
        button.centerYAnchor.constraint(equalTo: selectedLable.centerYAnchor).isActive = true
        button.widthAnchor.constraint(equalToConstant: 20).isActive = true
        button.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        return button
    }
    
    func makeDropDownMenu(titles: [String], selectedLable: UILabel) -> UIMenu {
        
        var menuAction: [UIAction] = []
        
        for title in titles {
            menuAction.append(UIAction(title: title) { (action) in
                selectedLable.text = "   " + title
            }
            )
        }
        let menu = UIMenu(title: "", options: .displayInline, children: menuAction)
        return menu
    }
}
