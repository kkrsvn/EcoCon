//
//  FilterViewExt.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 14.12.2022.
//

import UIKit

extension FilterViewController {
    func makeFilterView(cancelDelegate: CancelButtonDelegate, doneDelegate: DoneButtonDelegate) -> UIView {
        let view = FilterView()
        view.cancelButtonDelegate = cancelDelegate
        view.doneButtonDelegate = doneDelegate
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        return view
    }
}
