//
//  FilterViewController.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 14.12.2022.
//

import UIKit

protocol CancelButtonDelegate: AnyObject {
    func closeFilterView()
}

protocol DoneButtonDelegate: AnyObject {
    func acceptFilter()
}

class FilterViewController: UIViewController {
    
    var filterView: FilterView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.filterView = self.makeFilterView(cancelDelegate: self, doneDelegate: self) as? FilterView
    }
}
    
extension FilterViewController: CancelButtonDelegate, DoneButtonDelegate {
    func closeFilterView() {
        self.dismiss(animated: true)
    }
    
    func acceptFilter() {
        print("DONE!!!")
    }
}
