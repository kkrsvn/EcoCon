//
//  Categories.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 07.12.2022.
//

import Foundation

let categories: [(image: String, lable: String)] = [
    ("iphone.gen1", "Phones"),
    ("pc", "Computer"),
    ("bolt.heart", "Health"),
    ("book", "Books"),
    ("house", "House")
]
