//
//  CropImage.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 14.12.2022.
//

import UIKit

func cropingImage(targetImage: UIImage) -> UIImage {
    
        let xOffset = targetImage.size.width / 2
        let yOffset = 0
        let sideLength = targetImage.size.height
        
        let cropRect = CGRect(
            x: xOffset,
            y: CGFloat(yOffset),
            width: sideLength,
            height: sideLength
        ).integral
        
    let croppedCGImage = targetImage.cgImage!.cropping(to: cropRect)!
        
        let image: UIImage = UIImage(cgImage: croppedCGImage, scale: targetImage.scale, orientation: targetImage.imageOrientation)

        return image
    }
