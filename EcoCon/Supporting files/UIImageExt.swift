//
//  UIImageExt.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 10.12.2022.
//

import UIKit

extension UIImageView {
    func load(url: URL, croping: Bool) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = croping ? cropingImage(targetImage: image) : image
                    }
                }
            }
        }
    }
}
