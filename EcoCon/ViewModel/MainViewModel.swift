//
//  MainViewModel.swift
//  EcoCon
//
//  Created by Kirill Krasavin on 10.12.2022.
//

import UIKit

protocol MainViewModelProtocol: AnyObject {
    var networkService: NetworkServiceProtocol { get set }
    func updateMainView()
}

class MainViewModel: MainViewModelProtocol {
    
    var networkService: NetworkServiceProtocol
    
    required init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func updateMainView() {
        self.networkService.fetchWelcomeData(withURL: welcomeURL)
    }
}
